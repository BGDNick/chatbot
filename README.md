# Chatbot Project

## Overview

This repository hosts the code for a chatbot application utilizing Gradio for the interface and TheBloke/Llama-2-7B-Chat-GPTQ for the underlying model. The project aims to provide an interactive and user-friendly chatbot capable of understanding and generating human-like text responses.

## Features

- **Gradio Interface**: Allows users to interact with the chatbot through a simple web interface.
- **Llama-2-7B-Chat-GPTQ**: Leverages an advanced language model for generating accurate and contextually appropriate responses.

## Installation

### Prerequisites

Before you begin, ensure you have Python installed on your system. This project uses Python 3.9 or newer. You can download Python from [python.org](https://www.python.org/downloads/).

### Setup

To set up and run the project locally, follow these steps:

1. **Clone the Repository**

```bash
   git clone https://gitlab.com/BGDNick/chatbot.git
   cd chatbot
```
2. Install Poetry
```bash
pip install poetry
```
3. Install Dependencies
```bash
poetry install
```

## Usage

Once the application is running, open web browser with the URL provided by gradio